const { startTimer, stopTimer, countdownTimers, dataAuction } = require('./timer');

const handleSocketAuction = (socket, io) => {
  // console.log('A user connected');

  socket.on("join_room", (roomId) => {
    socket.join(roomId);
    if(dataAuction[roomId]){
      io.to(roomId).emit("receive_status", dataAuction[roomId]);
    }
    // console.log(`user with id-${socket.id} joined room - ${roomId}`);
   
    // Kirim waktu saat ini ke klien baru yang terhubung ke ruangan

  });

  socket.on('startTimer', ({ room, minutes, seconds }) => {
    if (!(room in countdownTimers)) {
      dataAuction[room] = {is_finished : 0, data:[]};
      countdownTimers[room] = { time: minutes * 60, interval: null };
      startTimer(room, io);
    }
  });

  socket.on('stopTimer', (room) => {
    if(countdownTimers[room] && "time" in countdownTimers[room]){
      stopTimer(room,io);
    }
  });

  socket.on("send_msg", (data) => {
    //This will send a message to a specific room ID
    socket.to(data.roomId).emit("receive_msg", data);
  });

  socket.on("delete_data_room", (roomId) => {
    //This will send a message to a specific room ID
    if(dataAuction[roomId]){
      io.to(roomId).emit("receive_status", dataAuction[roomId]);
      delete dataAuction[roomId];
    }
  });

  socket.on("send_bid", (data)=>{
    dataAuction[data.auction_id].data.push(data);
    io.to(data.auction_id).emit("receive_bid", dataAuction[data.auction_id]);
  })

  socket.on('disconnect', () => {
    // console.log('A user disconnected');
  });
  
};

module.exports = { handleSocketAuction };
