const countdownTimers = {};
const dataAuction = {};

const startTimer = (room, io) => {
  countdownTimers[room].interval = setInterval(() => {
    if (countdownTimers[room].time > 0) {
      dataAuction[room].is_finished = 0;
      countdownTimers[room].is_finished = 0;
      countdownTimers[room].time--;
      const minutes = Math.floor(countdownTimers[room].time / 60);
      const seconds = countdownTimers[room].time % 60;
      countdownTimers[room].timer = `${minutes}:${seconds.toString().padStart(2, '0')}`;
      io.to(room).emit('time', countdownTimers[room] );
    } else {
      countdownTimers[room].is_finished = 1;
      dataAuction[room].is_finished = 1;
      clearInterval(countdownTimers[room].interval);
      io.to(room).emit('time', countdownTimers[room] );
      delete countdownTimers[room];
    }
  }, 1000);
};

const stopTimer = (room,io) => {
  if (countdownTimers[room].interval) {
    clearInterval(countdownTimers[room].interval);
    countdownTimers[room].timer = "00:00";
    countdownTimers[room].is_finished = 1;
    dataAuction[room].is_finished = 1;
    io.to(room).emit('time', countdownTimers[room]);
    delete countdownTimers[room];
  }
};

const stopTimerBak = (room,io) => {
  dataAuction[room].is_finished = 1;
  if (countdownTimers[room].interval) {
    countdownTimers[room].timer = "00:00";
    countdownTimers[room].is_finished = 1;
    dataAuction[room].is_finished = 1;
    io.to(room).emit('time', countdownTimers[room]);
    clearInterval(countdownTimers[room].interval);
    delete countdownTimers[room];
  }
};

module.exports = { startTimer, stopTimer, countdownTimers, dataAuction };