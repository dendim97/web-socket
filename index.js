const http = require("http");
const { Server } = require("socket.io");
const cors = require("cors");
const { handleSocketAuction } = require('./socket/e_auction/socketHandler');
const httpServer = http.createServer();

const io = new Server(httpServer, {
  cors: {
    origin: "http://localhost:3000", // Replace with your frontend URL
    methods: ["GET", "POST"],
    allowedHeaders: ["my-custom-header"],
    credentials: true,
  },
});

io.on("connection", (socket) => {
    handleSocketAuction(socket, io);
});

const PORT = process.env.PORT || 2000 ;
httpServer.listen(PORT, () => {
  console.log(`Socket.io server is running on port ${PORT}`);
});